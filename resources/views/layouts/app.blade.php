<html>
    <head>
        <title>App Name - @yield('title')</title>
    </head>
    <body>
        <div class="container">
            @yield('cabeza')
        </div>

        <div class="container">
            @yield('content')
        </div>

        <div class="container">
            @yield('content1')
        </div>

    </body>
</html>